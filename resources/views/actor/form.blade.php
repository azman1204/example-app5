@extends('master')
@section('content')
<form action="/actor/store" method="post">
    <input type="hidden" name="actor_id" value="{{ $actor->actor_id }}">
    @csrf
    <div class="row">
        <div class="col-md-2">First Name</div>
        <div class="col-md-6">
            <input type="text" value="{{ $actor->first_name }}" class="form-control" name="first_name">
            @error('first_name') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">Last Name</div>
        <div class="col-md-6">
            <input type="text" value="{{ $actor->last_name }}" class="form-control" name="last_name">
            @error('last_name') <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-6"><input type="submit" class="btn btn-primary" value="Simpan"></div>
    </div>
</form>
@endsection