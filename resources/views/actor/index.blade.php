@extends('master')
@section('content')
@if (session()->has('msg-success'))
    <div class="alert alert-success">{{ session('msg-success') }}</div>
@endif
<a href="/actor/create" class="btn btn-primary">New Actor</a>
<table class="table table-bordered table-dark">
    <tr>
        <td>#</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Action</td>
    </tr>
    @php $no = $actors->firstItem(); @endphp
    @foreach($actors as $actor)
    <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $actor->first_name }}</td>
        <td>{{ $actor->last_name }}</td>
        <td>
            <a href="/actor/edit/{{ $actor->actor_id }}"><i class="bi-pencil"></i></a>
            <a href="/actor/delete/{{ $actor->actor_id }}" 
                onClick="return confirm('Anda pasti hendak hapus data ?')"><i class="bi-trash text-danger"></i></a>
        </td>
    </tr>
    @endforeach
</table>
{{ $actors->links() }}
@endsection