@extends('master')
@section('title', 'Laravel Training')

@section('sidebar')
    This is new section
@endsection



@section('content')

<table class='table table-dark table-striped'>
    <tr class='table-info'>
        <td>#</td>
        <td>Name</td>
        <td>Store</td>
    </tr>
    @foreach($customers as $cust)
    <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $cust->first_name }}</td>
        <td>{{ $cust->store->alamat->address }}</td>
    </tr>
    @endforeach
</table>
@endsection