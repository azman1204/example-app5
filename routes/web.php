<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ActorController;
use App\Http\Controllers\LoginController;


// example-app5.test
Route::get('/', function () {
    return view('welcome');
});

// param 1 = URL pattern, param2 = anonymous function
// example-app5.test/hello
Route::get('/hello', function() {
    echo 'Hello World';
});

// laravel 6,7
//Route::get('/user', 'UserController@index');
// laravel 8. example-app5.test/user
// laravelRoute::get('/user', [UserController::class, 'index']);
//Route::post('/user', [UserController::class, 'index']);
Route::any('/user', [UserController::class, 'index']);

// route parameter
Route::get('/user/{id}', function($id) {
    echo "Your id is $id";
});

Route::get('/user2/{id}/{nama?}', [UserController::class, 'index2'])->name('staff');

// run ini bila tiada route yg match
Route::fallback(function() {
    echo "Page tidak dijumpai!";
});

// ---------------------------------------------------------------------------------
// database (query builder dan ORM)
Route::get('/db1', function() {
    // query builder
    $actors = \DB::table('actor')->get(); // return array of obj
    foreach ($actors as $act) {
        echo $act->first_name . '<br>';
    }
});

Route::get('/db2', function() {
    // query builder (raw)
    $customers = \DB::select('select * from customer where customer_id = ?', [1]); // return array of obj
    foreach ($customers as $c) {
        echo $c->first_name . '<br>';
    }
});

Route::get('/db3', function() {
    // query builder
    // select * from actor where first_name = 'penelope'
    // tanpa ->get() OR first() at end, akan return Query obj
    $actor = \DB::table('actor')->where('first_name', 'penelope')->first(); // return an obj
    echo $actor->first_name . ' ' . $actor->last_name;
});

Route::get('/db4', function() {
    // query builder
    // select * from actor where first_name LIKE '%penelope%'
    // tanpa ->get() OR first() at end, akan return Query obj
    $actors = \DB::table('actor')->where('first_name', 'like', '%john%')->get(); // return an obj
    foreach ($actors as $a) {
        echo $a->first_name . ' ' . $a->last_name . '<br>';
        //echo "$a->first_name $a->last_name";
    }
});

// sample point to other connection
Route::get('/db5', function() {
    $users = \DB::connection('example')->table('users')->get(); // return an obj
    foreach ($users as $user) {
        echo $user->name . '<br>';
    }
});

Route::get('/db6', function() {
    $films = \DB::table('actor')
    ->select('title', 'first_name')
    ->join('film_actor', 'actor.actor_id', 'film_actor.actor_id')
    ->join('film', 'film.film_id', 'film_actor.film_id')
    ->orderBy('title')
    ->get(); // return an obj
    foreach ($films as $film) {
        echo $film->title . ' ' . $film->first_name . '<br>';
    }
});

Route::any('/film/search', 
[\App\Http\Controllers\FilmController::class, 'search']);

// model / Eloquent ORM
Route::get('/orm1', function() {
    $actors = \App\Models\Actor::all();
    foreach ($actors as $actor) {
        echo $actor->first_name . '<br>';
    }
});

// insert data guna ORM
Route::get('/orm2', function() {
    $actor = new \App\Models\Actor();
    $actor->first_name = 'Azman';
    $actor->last_name = 'Zakaria';
    $actor->save();
});

// update data guna ORM
Route::get('/orm3', function() {
    $actor = \App\Models\Actor::where('first_name', 'john')->first(); // an obj
    $actor->first_name = 'John';
    $actor->last_name = 'Doe';
    $actor->save();
    return "data dikemaskini";
});

// delete data guna ORM
Route::get('/orm4', function() {
    $actor = \App\Models\Actor::where('first_name', 'john')
    ->where('last_name', 'doe')->first(); // an obj
    $actor->delete();
    return "data dihapuskan";
});

Route::get('/relation1', function() {
    // list semua film dan text berkaitan
    $films = \App\Models\Film::take(10)->get();
    foreach($films as $f) {
        echo $f->title . ' / ' . $f->filmText->description . '<hr>';
    }
});

Route::get('/relation2', function() {
    // list semua film dan text berkaitan
    $stores = \App\Models\Store::all();
    foreach($stores as $store) {
        echo $store->alamat->address . ' ' . $store->alamat->district . '<br>';
        $customers = $store->customer; // ini return array of customers obj
        foreach($customers as $customer) {
            echo $customer->first_name . '<br>';
        }
        echo '<hr>';
    }
});

// inverse relation one to many
Route::get('/relation3', function() {
    $customers = \App\Models\Customer::take(10)->get();
    return view('relation3', compact('customers'));
});

// many to many
Route::get('/relation4', function() {
    $actor = \App\Models\Actor::find(1);
    $films = $actor->film;
    echo $actor->first_name . '<br>';
    foreach($films as $film) {
        echo $film->title . '<br>';
    }
});

// many to many (inverse)
Route::get('/relation5', function() {
    $film = \App\Models\Film::find(1);
    $actors = $film->actor;
    echo $film->title . '<br>';
    foreach($actors as $actor) {
        echo $actor->first_name . '<br>';
    }
});

Route::middleware(['pakguard'])->prefix('actor')->group(function() {
    Route::get('/', [ActorController::class, 'index']);
    Route::get('/create', [ActorController::class, 'create']);
    Route::post('/store', [ActorController::class, 'store']);
    Route::get('/edit/{id}', [ActorController::class, 'edit']);
    Route::get('/delete/{id}', [ActorController::class, 'delete']);
});

// generate password
Route::get('/password', function() {
    return \Hash::make('1234');
});

// login
Route::get('/login', [LoginController::class, 'index']);
Route::get('/logout', [LoginController::class, 'logout']);
Route::post('/login', [LoginController::class, 'auth']);

// email
Route::get('/mail', function() {
    \Mail::send('email.sample', ['nama' => 'Azman'], function($m) {
        $m->to('azman1204@gmail.com');
        $m->subject('Testing Email...');
    });
});

// spatie / authorization
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

Route::Get('/create-role', function() {
    $role = Role::create(['name' => 'writer']); // insert into roles table
    $permission = Permission::create(['name' => 'edit articles']); // insert dalam permissions table
    $role->givePermissionTo($permission); // insert into role_has_permission
});

// assign a role to a user
Route::get('/assign-role', function() {
    $user = \App\Models\User::find(1);
    $user->assignRole('writer'); // insert into table model_has_roles
});

// check role / permission of a user
Route::get('check-role', function() {
    $user = \App\Models\User::find(2);
    if($user->hasRole('writer')) {
        echo $user->name . " has writer role";
    } else {
        echo $user->name . ' has no writer role';
    }
});

Route::get('/check-role2', function() {
    return view('role');
});

// direct permission
Route::get('/direct-permission', function() {
    Permission::create(['name' => 'delete articles']);
    $user = \App\Models\User::find(1);
    $user->givePermissionTo('delete articles');
});

// PDF / DOMPDF
Route::get('pdf', function() {
    $pdf = \App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Test</h1>');
    return $pdf->stream();
});

Route::get('/pdf2', function() {
    // resources/views/pdf/invoice.blade.php
    $pdf = PDF::loadView('pdf.invoice', ['nama' => 'Azman Zakaria']);
    return $pdf->download('invoice.pdf');
});

// File Storage

// create file di storage/app/test.html
Route::get('/file1', function() {
    Storage::disk('local')->put('test.html', "<h1>Hello World</h1>");
});

// baca file content
Route::get('/file2', function() {
    $content = Storage::get('test.html');
    return $content;
});

// save as file
Route::get('/file3', function() {
    return Storage::download('test.html');
});

Route::view('/upload', 'form_upload'); // sortcut kpd. code d bawah. form_upload.blade.php
// Route::get('upload', function() {
//     return view('form_upload');
// });
Route::post('/upload2', function() {
    //$path = request()->file('avatar')->store('avatars');
    $name = request()->file('avatar')->getClientOriginalName();
    $path = request()->file('avatar')->storeAs('avatars', $name, 'public');
    echo 'File telah berjaya diupload di ' . $path;
});