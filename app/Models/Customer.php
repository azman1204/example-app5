<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Customer extends Model {
    protected $table = 'customer';
    public function store() {
        return $this->belongsTo(\App\Models\Store::class,
        'store_id', 'store_id');
    }
}
