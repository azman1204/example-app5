<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

// satu model mewakili satu table. ORM
// convention nama model singular, nama table plural
// tiap2 table mesti ada colum created_at dan updated_at
// primary ialah column bernama id
class Actor extends Model
{
    // config utk table yg tak plural
    protected $table = 'actor';
    // config utk table yg tak ada col. created_at dan updated_at
    public $timestamps = false;

    // config utk colum primary key yg bukan 'id'
    protected $primaryKey = 'actor_id';

    public function film() {
        return $this->belongsToMany(\App\Models\Film::class,
        'film_actor', 'actor_id', 'film_id');
    }
}
