<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Store extends Model {
    protected $table = 'store';
    public function customer() {
        return $this->hasMany(\App\Models\Customer::class, 
        'store_id', 'store_id');
    }

    public function alamat() {
        return $this->belongsTo(\App\Models\Address::class,
        'address_id', 'address_id');
    }
}
