<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Film extends Model {
    protected $table = 'film';
    protected $primaryKey = 'film_id';
    // one to one relationship
    public function filmText() {
        return $this->hasOne(\App\Models\FilmText::class, 'film_id', 'film_id');
    }

    public function actor() {
        return $this->belongsToMany(\App\Models\Actor::class,
        'film_actor', 'film_id', 'actor_id');
    }
}
