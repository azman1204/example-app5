<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PakGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        // if(! $request->has('age')) {
        //     echo "Age is required";
        //     exit;
        // }

        // if ($request->age < 40) {
        //     echo "Life is not yet begin";
        //     exit;
        // }

        // check user dah login atau belum
        // \Auth::check() - return true kalau user dah login
        if(! \Auth::check()) {
            return redirect('/login');
        }
        return $next($request);
    }
}
