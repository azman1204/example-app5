<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller {
    // show login form
    public function index() {
        return view('login.form');
    }

    public function auth(Request $req) {
        $arr = $req->only('nokp', 'password');
        if(\Auth::attempt($arr)) {
            // echo 'Berjaya';
            return redirect('/actor');
        } else {
            // echo 'tak berjaya';
            return redirect('/login')->with('msg', 'Login Gagal');
        }
    }

    public function logout() {
        \Auth::logout();
        session()->flush(); // delete semua session
        return redirect('/login');
    }
}
