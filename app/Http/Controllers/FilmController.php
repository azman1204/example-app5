<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
class FilmController extends Controller {
    // carian film by title / actor combination
    public function search(Request $req) {
        if ($req->isMethod('post')) {
            //dd($req->all()); // dumpdie. all() return semua data dr form
            // click btn cari
            //$films = \DB::table('film')->where('title', 'like', "%$req->title%")->get();
            $query = \DB::table('film')
            ->join('film_actor', 'film.film_id', 'film_actor.film_id')
            ->join('actor', 'actor.actor_id', 'film_actor.actor_id');
            
            if(! empty($req->title)) {
                $query = $query->where('title', 'like', "%$req->title%");
            }

            if(! empty($req->name)) {
                $query = $query->where('first_name', 'like', "%$req->name%");
            }

            $films = $query->get();
            return view('film.search', ['films' => $films]);
            //return view('film.search', compact('films', 'lain2 var'));
        } else {
            // click menu carian
            return view('film.search');
        }
    }
}
