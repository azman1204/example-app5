<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class UserController extends Controller {
    public function index(Request $req) {
        // passing data dr controller ke view
        // guna 2nd parameter function view()
        if($req->isMethod('post')) {
            $nama = $req->nama;
        } else {
            $nama = 'Anonymous';
        }
        return view('user.index', ['nama' => $nama]); 
        // resources/views/user/index.blade.php
    }

    public function index2($id, $nama = 'Anonymous') {
        echo "ID Anda ialah $id dan nama anda $nama";
        //return redirect()->route('staff');
    }
}