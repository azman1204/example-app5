<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Actor;

class ActorController extends Controller {
    // list all actors
    public function index() {
        $actors = Actor::paginate(20);// query / retrieve
        return view('actor.index', compact('actors'));
    }

    // create an actor
    public function create() {
        $actor = new Actor();
        return view('actor.form', compact('actor'));
    }

    // insert dan update
    public function store(Request $req) {
        $actor_id = $req->actor_id;
        if(! empty($actor_id)) {
            // update
            $actor = Actor::find($actor_id);
        } else {
            // new / insert
            $actor = new Actor();
        }
        
        $actor->first_name = $req->first_name;
        $actor->last_name = $req->last_name;

        // validation
        $rules = ['first_name' => 'required', 'last_name' => 'required'];
        $v = \Validator::make($req->all(), $rules);
        if($v->passes()) {
            // validation ok
            $actor->save();
            session(['actor_id' => $actor->actor_id]); // set data ke dlm session
            return redirect('/actor')->with('msg-success', 'Data telah berjaya disimpan');
            // with('key', 'val') = set flash session
        } else {
            // validation not ok
            return view('actor.form', compact('actor'))->withErrors($v);
        }
    }

    // edit actor
    public function edit($id) {
        $actor = Actor::find($id);
        return view('actor.form', compact('actor'));
    }

    // delete actor
    public function delete($id) {
        Actor::find($id)->delete();
        return redirect('/actor');
    }
}
